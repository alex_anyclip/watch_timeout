"""Usage:
        watch_timeout.py --url=<trafficamanager_url> --timeout=<request_timeout> --payload=<request_json> --sleep=<sleep_between_requests>

Options:
  -h --help
  -u --url=<trafficmanager_url>
  -t --timeout=<timeout>
  -s --sleep=<sleep_between_requests>
  -p --payload=<payload>
"""
import json
import logging
from os import environ
from time import sleep
from typing import Dict

import requests
from datadog import initialize, api
from docopt import docopt
from logentries import LogentriesHandler
from requests import Response, Session
from requests.adapters import HTTPAdapter
from requests.auth import HTTPBasicAuth, HTTPDigestAuth
from requests_ntlm import HttpNtlmAuth
from string_utils import is_full_string
from urllib3 import Retry
from urllib3.exceptions import NewConnectionError

LOGENTRIES_TEMPLATE = '[{}] %(asctime)s [%(process)d|%(threadName)s] [%(levelname)-5.5s] [%(module)s:%(lineno)s:%(funcName)s] %(message)s'
CONSOLE_TEMPLATE = '%(asctime)s [%(process)d|%(threadName)s] [%(levelname)-5.5s] [%(module)s:%(lineno)s:%(funcName)s] %(message)s'
formatter = logging.Formatter(LOGENTRIES_TEMPLATE.format('watch_timeout_script'))

log = logging.getLogger()

logentries_handler = LogentriesHandler('f7684009-906b-49bf-8862-db9539ff3ec3')
logentries_handler.setFormatter(formatter)
logentries_handler.setLevel(logging.ERROR)

console_handler = logging.StreamHandler()
console_handler.setFormatter(logging.Formatter(CONSOLE_TEMPLATE))

log.addHandler(logentries_handler)
log.addHandler(console_handler)
log.setLevel(logging.INFO)

namespace = 'video_upload.'


def get_env(key, default=None):
    return environ.get(key) or default


dd_tags = [
    'env:' + (environ.get('ENV') or ''),
    'role:' + (environ.get('ROLE') or ''),
    'hostname:' + (environ.get('HOSTNAME') or '')
]


def _set_tags(tags):
    if tags:
        tags += dd_tags
    else:
        tags = dd_tags
    return tags


class Priority:
    normal = 'normal'


class AlertType:
    info = 'info'
    warning = "warning"
    error = "error"


class DDEvent:
    tags = dd_tags

    def __init__(self, title: str, text: str, tags=(), alert_type=AlertType.error) -> None:
        self.title = title
        self.tags = _set_tags(tags)
        self.text = text
        self.alert_type = alert_type

    def data(self) -> Dict:
        return {
            "title": self.title,
            "text": self.text,
            "tags": self.tags,
            "alert_type": self.alert_type
        }


class DatadogClient:

    def __init__(self):
        self._init()

    @staticmethod
    def _init():
        initialize(api_key='39ddaf958f156ca857e0a5d86d013814',
                   app_key='43b684cf0843f44d90f5e3d6e51d70cb7c231f7e',
                   api_host='https://api.datadoghq.com',
                   hostname_from_config=False)

    def increment(self, metric_name, tags=None):
        try:
            tags = _set_tags(tags)
            api.Metric.send(metric=namespace + metric_name, points=1, tags=tags, type='count')
        except:
            pass

    def timer(self, metric_name, value, tags=None):
        try:
            tags = _set_tags(tags)
            api.Metric.send(metric=namespace + metric_name, points=value, tags=tags, type='gauge')
        except:
            pass

    def submit(self, metrics: list, tags=None):
        try:
            tags = _set_tags(tags)
            for metric in metrics:
                metric['metric'] = namespace + metric['metric']
                metric['tags'] = tags
            api.Metric.send(metrics=metrics, tags=tags)
        except:
            pass

    def send_event(self, event: DDEvent) -> str:
        response = api.Event.create(title=event.title,
                                    text=event.text,
                                    tags=event.tags,
                                    alert_type=event.alert_type
                                    )
        if 'status' not in response or response.get('status') != 'ok':
            log.error(f'Unable to send DD event. {response}. Event: {event.data()}')
        return response


class HttpClient:
    ERROR_UNKNOWN = 'Unknown error'

    default_headers = {'User-Agent':
                           'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                           'Chrome/75.0.3770.142 Safari/537.36',
                       'Accept-Encoding': ', '.join(('gzip', 'deflate')),
                       'Accept': '*/*',
                       'Connection': 'keep-alive',
                       }

    status_forcelist = (429, 500, 502, 503, 504)
    method_whitelist = ('HEAD', 'TRACE', 'GET', 'PUT', 'OPTIONS', 'DELETE', 'POST')

    def __init__(self, username=None, password=None, auth_type=None, session=None, retry=None, headers=None):
        self.username = username
        self.password = password
        self.auth_type = auth_type
        self.headers = headers or self.default_headers
        self._session = session or requests.Session()
        self.retry = retry or Retry(total=3, backoff_factor=2, status_forcelist=self.status_forcelist,
                                    allowed_methods=self.method_whitelist, raise_on_status=True)
        self._init_session()

    def _init_session(self):
        adapter = HTTPAdapter(max_retries=self.retry)
        self._session.mount('http://', adapter)
        self._session.mount('https://', adapter)
        self._session.headers = self.headers
        if self.auth_type:
            if not self.username or not self.password:
                raise ValueError(
                    f'Username or password is not set. Username: "{self.username}" Password: "{self.password}"')

            auth = AuthType.get_by_string(self.auth_type)
            self._session.auth = auth(self.username, self.password)

    def get_session(self) -> Session:
        return self._session

    @staticmethod
    def get_error(response: Response) -> str:
        if is_full_string(response.reason):
            reason = f' - {response.reason}'
        else:
            reason = ''
        if is_full_string(response.text):
            text = f' : {response.text}'
        else:
            text = ''
        return f'{response.status_code}{reason}{text}' if response is not None else HttpClient.ERROR_UNKNOWN


class AuthType:
    basic = HTTPBasicAuth
    ntlm = HttpNtlmAuth
    dig = HTTPDigestAuth
    digest = HTTPDigestAuth

    @staticmethod
    def get_by_string(auth_type: str):
        return AuthType.__dict__.get(auth_type)


if __name__ == '__main__':
    dd_client = DatadogClient()
    log.info(docopt(__doc__))
    url = docopt(__doc__)['--url']
    payload = json.loads(docopt(__doc__)['--payload'])
    timeout = int(docopt(__doc__)['--timeout']) or 15
    sleep_sec = int(docopt(__doc__)['--sleep']) or 10
    iterations = 0
    while True:
        with HttpClient().get_session() as session:
            response = None
            try:
                sleep(sleep_sec)
                iterations += 1
                log.info(f'Iteration {iterations}')
                response = session.post(url, json=payload, timeout=timeout)
                status_code = response.status_code
                if status_code not in (200, 204):
                    log.error(f'watch_timeout.py Invalid status code: {status_code}. Response: {response.text}')
                    continue
                if status_code == 204:
                    continue
                response_json = response.json()
                playlists = response_json.get('playlists')
                if not playlists or len(playlists) < 1:
                    log.error(f'watch_timeout.py playlists size is 0 or None: {playlists}')
            except requests.exceptions.ConnectionError as e:
                dd_event = DDEvent('Watch timeout issue', f'Retries have been exhausted: {e}',
                                   tags=['env: watch_timeout'])
                log.exception('watch_timeout.py Retries have been exhausted')
                dd_client.send_event(dd_event)
                continue
            except NewConnectionError as e:
                dd_event = DDEvent('Watch timeout issue', f'Unable to connect to TM: {e}',
                                   tags=['env: watch_timeout'])
                log.exception('watch_timeout.py Unable to connect to TM')
                dd_client.send_event(dd_event)
                continue
            except json.decoder.JSONDecodeError:
                log.exception(f'watch_timeout.py Unable to parse Trafficmanager response: "{response.text}"')
                continue
            except Exception as e:
                dd_event = DDEvent('Watch timeout issue',
                                   f'Exception caught while execution: {e}. Trafficmanager Response: "{response.text}"',
                                   tags=['env: watch_timeout'])
                log.exception(f'watch_timeout.py Exception caught while execution. '
                              f'Trafficmanager Response: {response.text}')
                dd_client.send_event(dd_event)
                continue
